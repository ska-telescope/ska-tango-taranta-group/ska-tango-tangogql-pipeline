FROM condaforge/mambaforge as mamba

COPY tangogql/environment.yml /environment.yml
RUN mamba env create -p /env -f environment.yml \
  && conda clean -afy

FROM debian:11-slim

# curl is used by the livenessProbe on Kubernetes deployment
RUN apt-get update \
  && apt-get install -y curl make git \
  && rm -rf /var/lib/apt/lists/*

EXPOSE 5004

COPY --from=mamba /env /env

WORKDIR /tangogql
COPY . .
COPY tangogql/ ./

ENV SECRET=$SECRET
ENV PYTHONUNBUFFERED 1
ENV PYTANGO_GREEN_MODE asyncio

RUN sed -i 's/"secret":""/"secret":"'${SECRET}'"/g' tangogql/config.json

ENV PATH /env/bin:$PATH

RUN chmod 777 /tangogql \
&& pip install pytest-cov

CMD ["python3", "-m", "tangogql"]